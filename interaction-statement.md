# Interaction Statement

The only thing users will have to do is randomly press any key on the keyboard throughout this whole sketch.

In the beginning, they will see a normal image. There will be intermittent glitches with sound to invite users to further interact with it. When a user presses any key, a glitch occurs with a different sound. This is to show users that their input is somehow impacting the piece. Users will naturally try other keys, and even mashing some keys pretty quickly. They will soon realise the more they press keys, the heavier the glitches get.

Once a glitch gets very heavy, a more violent glitch sound will play. This sound indicates that they are progressing, but also is meant to show that they are 'breaking' the piece even more. This is important as if they maintain this heavy glitch, they will 'break' it to the point the image transitions to another conflicting image. Users should realise that they are forcing change is breaking the subject in the photos.

This interaction repeats again for each image. After awhile, users hands will get tired of mashing keys again and again, making it more difficult as the sketch progresses. Users should realise that this change they are forcing is very difficult.

On the final image, when the user tries to transition to the next image, it will instead cause the sketch to show a 'shut-down' transition, leaving a blank screen for a few seconds. This abrupt shut-down is to surprise users, showing that all the glitches they have been doing has finally 'broken' the sketch. The blank screen (if viewed on a monitor/TV) should show the reflection of the user. This blank screen would allow the user to reflect on the piece and what it means, and also show that these difficulties can happen to them too.

The interaction will loop again from the first image and process can repeat.

(For marking purposes (may affect experience): use + and - to quickly transition)

Works on Ubuntu Lab Machines in CSIT. Encountered loading error on windows machine.
