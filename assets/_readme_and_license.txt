Sound pack downloaded from Freesound.org
----------------------------------------

This pack of sounds contains sounds by the following user:
 - dotY21 ( https://freesound.org/people/dotY21/ )

You can find this pack online at: https://freesound.org/people/dotY21/packs/16933/

License details
---------------

Creative Commons 0: http://creativecommons.org/publicdomain/zero/1.0/
Attribution Noncommercial: http://creativecommons.org/licenses/by-nc/3.0/


Sounds in this pack
-------------------

  * 384132__doty21__malfunction-static.wav
    * url: https://freesound.org/s/384132/
    * license: Creative Commons 0
  * 376700__doty21__digital-madness.wav
    * url: https://freesound.org/s/376700/
    * license: Creative Commons 0
  * 376701__doty21__tech-corrupt.wav
    * url: https://freesound.org/s/376701/
    * license: Creative Commons 0
  * 371183__doty21__corrupted-static-noise-loopable.wav
    * url: https://freesound.org/s/371183/
    * license: Creative Commons 0
  * 362510__doty21__crackle-glitch.wav
    * url: https://freesound.org/s/362510/
    * license: Creative Commons 0
  * 353209__doty21__digital-screams.wav
    * url: https://freesound.org/s/353209/
    * license: Creative Commons 0
  * 349900__doty21__digital-glitches.wav
    * url: https://freesound.org/s/349900/
    * license: Creative Commons 0
  * 349899__doty21__digital-error.wav
    * url: https://freesound.org/s/349899/
    * license: Creative Commons 0
  * 348585__doty21__glitch-corruption.wav
    * url: https://freesound.org/s/348585/
    * license: Creative Commons 0
  * 347698__doty21__electric-static-interference-sound.wav
    * url: https://freesound.org/s/347698/
    * license: Creative Commons 0
  * 333175__doty21__glitchy-static-3.wav
    * url: https://freesound.org/s/333175/
    * license: Creative Commons 0
  * 333173__doty21__system-sweep.wav
    * url: https://freesound.org/s/333173/
    * license: Creative Commons 0
  * 331733__doty21__system-interruption-corrupted-signal.wav
    * url: https://freesound.org/s/331733/
    * license: Creative Commons 0
  * 331638__doty21__glitchy-musical.wav
    * url: https://freesound.org/s/331638/
    * license: Creative Commons 0
  * 328697__doty21__glitchy-static-2.wav
    * url: https://freesound.org/s/328697/
    * license: Creative Commons 0
  * 325851__doty21__glitchy-static.wav
    * url: https://freesound.org/s/325851/
    * license: Creative Commons 0
  * 320569__doty21__static-error-2.wav
    * url: https://freesound.org/s/320569/
    * license: Creative Commons 0
  * 320566__doty21__glitch-sound-2.wav
    * url: https://freesound.org/s/320566/
    * license: Creative Commons 0
  * 317486__doty21__short-glitch-4.wav
    * url: https://freesound.org/s/317486/
    * license: Creative Commons 0
  * 317485__doty21__long-glitch-4.wav
    * url: https://freesound.org/s/317485/
    * license: Creative Commons 0
  * 277053__doty21__glitch-sound-1.wav
    * url: https://freesound.org/s/277053/
    * license: Creative Commons 0
  * 277052__doty21__long-glitch-3.wav
    * url: https://freesound.org/s/277052/
    * license: Creative Commons 0
  * 275647__doty21__long-glitch-2.wav
    * url: https://freesound.org/s/275647/
    * license: Creative Commons 0
  * 275646__doty21__static-error-1.wav
    * url: https://freesound.org/s/275646/
    * license: Creative Commons 0
  * 275645__doty21__long-glitch-1.wav
    * url: https://freesound.org/s/275645/
    * license: Creative Commons 0
  * 275644__doty21__short-glitch-3.wav
    * url: https://freesound.org/s/275644/
    * license: Creative Commons 0
  * 275643__doty21__short-glitch-2.wav
    * url: https://freesound.org/s/275643/
    * license: Creative Commons 0
  * 274728__doty21__short-glitch.wav
    * url: https://freesound.org/s/274728/
    * license: Attribution Noncommercial


