# Statement of Originality

I, Septian Razi, declare that everything I have submitted in this major
project is entirely my own work, with the following exceptions:

## Inspiration
- Inspired by glitch art
## Code

## Assets
- Glitch sounds were taken from freeSound.org: <https://freesound.org/people/dotY21/packs/16933/> see "_readme_and_license.txt" file for licensing details

- Images taken by Konstantinos Katsanis directed by Septian Razi
