# Artist Statement

*Theme Chosen : The role of migration in transforming society*

Change is hard. As a person who has migrated from Indonesia to New Zealand, then back to Indonesia, and then to Australia, I have gone through many different experiences, from culture shock, homesickness and at times, confusion about my identity. As migration becomes more and more common in the future, people and governments must understand that migration is not always easy and happy, there are difficulties that these migrators must face when they migrate; such as their identity and change.

Indonesia and Australia/New Zealand are two very different environments. Moving from one to the other, I had to make many changes to my self, from my attitude, behaviour, appearance and my sense of identity in order to best assimilate into these environments.

With this piece I want to highlight how hard this process of change is and how I believe this is one of the major factors preventing migrants from assimilating into their new environments. This difficulty stems from; the fragility of identity, being forced to unnaturally change, and making compromises with or even completely abandoning old identities.

The pictures represent an identity and appear in order such that the next image will be of a conflicting identity (eg: Praying, then Drinking). Glitches in the pictures represent the unnatural fragility of these identities.

To change to a different image, players must work hard to progress, mashing keys, highlighting the difficulty of this change in the real world. These key mashes causes even more glitches as they unnaturally attempt to change the pictures.

The pictures finally transition when the glitch has become too random to decipher the original picture, which represents how forcing change can lead to the 'death' of your old identity. In it's place is a picture of a conflicting identity, only visible when the user 'destroys' the old picture in a fury of key presses and glitches. This is to show that compromises need to be made with your old identity in order to adopt a new one - sometimes even abandoning the old one. 

After the user goes through all the images, the last glitch transition ends with an abrupt TV screen shut-down. Sometimes forcing change - which is what the users are metaphorically doing - can lead to many complications, a 'shut-down' of ones sense of home and identity. In addition, if the user is looking at the piece on a monitor/TV, the abrupt shut-down will cause the screen to go blank, with a reflection of the user's self on the screen, highlighting that this difficulty can be experienced by anyone.

From this piece I hope the users, especially users who have never faced such a change in environment, understand that assimilation is not an easy process and future governments and leaders must take this into account as migration becomes increasingly more common in our globalised world.
